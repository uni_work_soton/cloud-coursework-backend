import logging
import azure.functions as func
import azure.cosmos as cosmos
import json

import os 
db_URI = os.environ['db_URI']
db_id = os.environ['db_id']
db_key = os.environ['db_key']
prompts_cont = os.environ['prompts_container']
player_cont = os.environ['player_container']

def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Prompt delete http request recieved')

    client = cosmos.cosmos_client.CosmosClient(db_URI, db_key)
    db_client = client.get_database_client(db_id)
    prompts_container = db_client.get_container_client(prompts_cont)
    player_container = db_client.get_container_client(player_cont)

    req_body = req.get_json()
    prompt_id = str(req_body["id"])
    username = req_body["username"]
    password = req_body["password"]

    all_players = list(player_container.query_items(
        query= "SELECT p FROM p WHERE p.id = @id",
        parameters = [{"name" : "@id", "value":username}],
        enable_cross_partition_query=True
    ))

    all_prompts_of_username = list(prompts_container.query_items(
        query= "SELECT p FROM p WHERE p.username = @username",
        parameters = [{"name" : "@username", "value":username}],
        enable_cross_partition_query=True
    ))

    all_prompts = list(prompts_container.query_items(
        query= "SELECT p FROM p",
        enable_cross_partition_query=True
    ))

    #check username and password are ok
    if all_players:
        if password != all_players[0]["p"]["password"]:
            logging.error("password doesnt match username")
            return func.HttpResponse(json.dumps({"result": False, "msg": "bad username or password" }))
    else:
        logging.error("username not found")
        return func.HttpResponse(json.dumps({"result": False, "msg": "bad username or password" }))

    #check if prompt id exists
    ids = []
    for prompt in all_prompts:
        ids.append(prompt["p"]["id"])

    logging.info(ids)
    
    if prompt_id not in ids:
        logging.error("prompt doesnt exist")
        return func.HttpResponse(json.dumps({"result": False, "msg": "prompt id does not exist" }))

    #check that prompt id is one of username's prompts

    ids = []
    for prompt in all_prompts_of_username:
        ids.append(prompt["p"]["id"])
    
    if prompt_id not in ids:
        logging.error("prompt doesnt belong to username")
        return func.HttpResponse(json.dumps({"result": False, "msg": "access denied" }))

    #ok!
    prompt_to_delete = [prompt for prompt in all_prompts_of_username if prompt["p"]["id"] == prompt_id][0]["p"]
    prompts_container.delete_item(prompt_to_delete,prompt_id)

    return func.HttpResponse(json.dumps({"result" : True,"msg": "OK" }))
