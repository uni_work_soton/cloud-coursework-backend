import unittest
import azure.cosmos as cosmos

import wrapper


import os
db_URI = os.environ['db_URI']
db_id = os.environ['db_id']
db_key = os.environ['db_key']

local = True

class TestPromptsGetText(unittest.TestCase):
    client = cosmos.cosmos_client.CosmosClient(db_URI, db_key )
    db_client = client.get_database_client(db_id)
    player_container = db_client.get_container_client('players')
    prompt_container = db_client.get_container_client('prompts')

    @classmethod
    def setUpClass(self) -> None:
        players = self.player_container.read_all_items()
        for p in players:
            self.player_container.delete_item(p['id'],p['id'])
        wrapper.player_register({'username': 'username',  'password': 'password' },local)
   
    def setUp(self) -> None:
        prompts = self.prompt_container.read_all_items()
        for p in prompts:
            self.prompt_container.delete_item(p['id'],p['id'])
    

    def test_getText_spaces(self) :
        wrapper.prompt_create({'text': 'this is a singular prompt','username': 'username', 'password': 'password'},local)
        wrapper.prompt_create({'text': 'This is one of many prompts','username': 'username', 'password': 'password'},local)

        resp = wrapper.prompts_getText({'word': 'prompt', 'exact' : False},local)
        respExact = wrapper.prompts_getText({'word': 'This', 'exact' : True},local)

        self.assertEqual(len(resp), 1)
        self.assertEqual(resp[0]['text'], 'this is a singular prompt')

        self.assertEqual(len(respExact), 1)
        self.assertEqual(respExact[0]['text'], 'This is one of many prompts')

    def test_getText_punctuation(self) :
        wrapper.prompt_create({'text': 'this, is a singular prompt!','username': 'username', 'password': 'password'},local)
        wrapper.prompt_create({'text': 'This is a another prompt.','username': 'username', 'password': 'password'},local)
        wrapper.prompt_create({'text': 'There are many prompts in this: one, two and three','username': 'username', 'password': 'password'},local)

        resp = wrapper.prompts_getText({'word': 'prompt', 'exact' : False},local)
        respExact = wrapper.prompts_getText({'word': 'this', 'exact' : True},local)

        self.assertEqual(len(resp), 2)
        self.assertEqual(resp[0]['text'], 'this, is a singular prompt!')
        self.assertEqual(resp[1]['text'], 'This is a another prompt.')

        self.assertEqual(len(respExact), 2)
        self.assertEqual(respExact[0]['text'], 'this, is a singular prompt!')
        self.assertEqual(respExact[1]['text'], 'There are many prompts in this: one, two and three')