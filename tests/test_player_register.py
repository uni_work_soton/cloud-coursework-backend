import unittest
import azure.cosmos as cosmos

import wrapper

import os
db_URI = os.environ['db_URI']
db_id = os.environ['db_id']
db_key = os.environ['db_key']

local = True

class TestPlayerRegister(unittest.TestCase):
    client = cosmos.cosmos_client.CosmosClient(db_URI, db_key )
    db_client = client.get_database_client(db_id)
    player_container = db_client.get_container_client('players')
   
    def setUp(self) -> None:
        players = self.player_container.read_all_items()
        for p in players:
            self.player_container.delete_item(p['id'],p['id'])

    def test_register_correct(self):
        payload = {'username': 'correctUsername', 'password': 'correctPassword'}
        resp = wrapper.player_register(payload,local)

        #check the response is correct
        self.assertTrue(resp['result'])
        self.assertEqual(resp['msg'],"OK")

        #check value is in database
        players = list(self.player_container.read_all_items())
        self.assertEqual(len(players),1)
        self.assertEqual(players[0]['id'],"correctUsername")
        self.assertEqual(players[0]['password'],"correctPassword")     

    def test_register_short_username(self):
        payload = {'username': 'lil', 'password': 'password'}
        resp = wrapper.player_register(payload,local)

        #check the response is correct
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"Username less than 4 characters or more than 16 characters")

        #check value is NOT in database
        players = list(self.player_container.read_all_items())
        self.assertEqual(len(players),0)   

    def test_register_long_username(self):
        payload = {'username': 'bbbbbbbbbbbbbbbbbbbbiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiigggggggggggggggggggggg', 'password': 'password'}
        resp = wrapper.player_register(payload,local)

        #check the response is correct
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"Username less than 4 characters or more than 16 characters")

        #check value is NOT in database
        players = list(self.player_container.read_all_items())
        self.assertEqual(len(players),0) 

    def test_register_short_password(self):
        payload = {'username': 'username', 'password': 'lil'}
        resp = wrapper.player_register(payload,local)

        #check the response is correct
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"Password less than 8 characters or more than 24 characters")

        #check value is NOT in database
        players = list(self.player_container.read_all_items())
        self.assertEqual(len(players),0)   

    def test_register_long_password(self):
        payload = {'username': 'username', 'password': 'bbbbbbbbbbbbbbbbbbbbiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiigggggggggggggggggggggg'}
        resp = wrapper.player_register(payload,local)

        #check the response is correct
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"Password less than 8 characters or more than 24 characters")

        #check value is NOT in database
        players = list(self.player_container.read_all_items())
        self.assertEqual(len(players),0)
    
    def test_register_already_registered(self):
        payload = {'username': 'correctUsername', 'password': 'correctPassword'}
        wrapper.player_register(payload,local)
        resp = wrapper.player_register(payload,local)

        #check the response is correct
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"Username already exists")

        #check value is not in database twice
        players = list(self.player_container.read_all_items())
        self.assertEqual(len(players),1)

if __name__ == '__main__':
    unittest.main()