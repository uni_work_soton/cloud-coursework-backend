import unittest
import azure.cosmos as cosmos

import wrapper

import os
db_URI = os.environ['db_URI']
db_id = os.environ['db_id']
db_key = os.environ['db_key']

local = True

class TestPromptCreate(unittest.TestCase):
    client = cosmos.cosmos_client.CosmosClient(db_URI, db_key )
    db_client = client.get_database_client(db_id)
    player_container = db_client.get_container_client('players')
    prompt_container = db_client.get_container_client('prompts')

    @classmethod
    def setUpClass(self) -> None:
        players = self.player_container.read_all_items()
        for p in players:
            self.player_container.delete_item(p['id'],p['id'])
        wrapper.player_register({'username': 'username', 'password': 'password'},local)
   
    def setUp(self) -> None:
        prompts = self.prompt_container.read_all_items()
        for p in prompts:
            self.prompt_container.delete_item(p['id'],p['id'])


    def test_create_correct(self):
        payload = {'text': 'This is a correctly formatted prompt!', 'username': 'username', 'password': 'password'}
        resp = wrapper.prompt_create(payload,local)

        #check the response is correct
        self.assertTrue(resp['result'])
        self.assertEqual(resp['msg'],"OK")

        #check value is in database
        prompts = list(self.prompt_container.read_all_items())
        self.assertEqual(len(prompts),1)
        self.assertEqual(prompts[0]['text'],"This is a correctly formatted prompt!")
        self.assertEqual(prompts[0]['username'],"username")     

    def test_create_duplicate(self):
        payload = {'text': 'This is a correctly formatted prompt!', 'username': 'username', 'password': 'password'}
        wrapper.prompt_create(payload,local)
        resp = wrapper.prompt_create(payload,local)

        #check the response is correct
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"This user already has a prompt with the same text")

        #check value is NOT in database twice
        prompts = list(self.prompt_container.read_all_items())
        self.assertEqual(len(prompts),1)

    def test_create_long(self):
        payload = {'text':'this is a wayyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy toooooooooooooooooooo lonnnnnnnnnnnnnggggggggggggggggg', 'username': 'username', 'password': 'password'}
        resp = wrapper.prompt_create(payload,local)

        #check the response is correct
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"prompt length is <20 or > 100 characters")

        #check value is NOT in database
        prompts = list(self.prompt_container.read_all_items())
        self.assertEqual(len(prompts),0)

    def test_create_short(self):
        payload = {'text':'this is a too short','username': 'username', 'password': 'password'}
        resp = wrapper.prompt_create(payload,local)

        #check the response is correct
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"prompt length is <20 or > 100 characters")

        #check value is NOT in database
        prompts = list(self.prompt_container.read_all_items())
        self.assertEqual(len(prompts),0)  

    def test_create_bad_user(self):
        payload = {'text': 'This is a correctly formatted prompt!', 'username': 'NOTusername', 'password': 'password'}
        resp = wrapper.prompt_create(payload,local)

        #check the response is correct
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"bad username or password")

        #check value is NOT in database
        prompts = list(self.prompt_container.read_all_items())
        self.assertEqual(len(prompts),0)  
    
    def test_create_bad_pass(self):
        payload = {'text': 'This is a correctly formatted prompt!', 'username': 'username', 'password': 'NOTpassword'}
        resp = wrapper.prompt_create(payload,local)

        #check the response is correct
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"bad username or password")

        #check value is NOT in database
        prompts = list(self.prompt_container.read_all_items())
        self.assertEqual(len(prompts),0)  

if __name__ == '__main__':
    unittest.main()