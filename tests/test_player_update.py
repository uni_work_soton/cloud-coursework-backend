import unittest
import azure.cosmos as cosmos

import wrapper


import os
db_URI = os.environ['db_URI']
db_id = os.environ['db_id']
db_key = os.environ['db_key']

local = True

class TestPlayerUpdate(unittest.TestCase):
    client = cosmos.cosmos_client.CosmosClient(db_URI, db_key )
    db_client = client.get_database_client(db_id)
    player_container = db_client.get_container_client('players')
   
    def setUp(self) -> None:
        for p in self.player_container.read_all_items():
            self.player_container.delete_item(p['id'],p['id'])
        payload = {'username': 'username', 'password': 'password'}
        wrapper.player_register(payload,local)
    
    def getPlayers(self):
        return list(self.player_container.query_items(
            query = "SELECT p FROM p WHERE p.id = @username",
            parameters = [{"name" : "@username" , "value" : "username"}],
            enable_cross_partition_query = True
        ))
    
    def test_update_played_correct(self):
        payload = {'username': 'username', 'password': 'password', 'add_to_games_played': 10}
        resp = wrapper.player_update(payload,local)
        self.assertTrue(resp['result'])
        self.assertEqual(resp['msg'],"OK")
        self.assertEqual(self.getPlayers()[0]['p']['games_played'],10)
    
    def test_update_played_wrong_user(self):
        payload = {'username': 'NOTusername', 'password': 'password', 'add_to_games_played': 10}
        resp = wrapper.player_update(payload,local)
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"user does not exist")
        self.assertEqual(self.getPlayers()[0]['p']['games_played'],0) #make sure old entry hasnt been update
    
    def test_update_played_wrong_pass(self):
        payload = {'username': 'username', 'password': 'NOTpassword', 'add_to_games_played': 10}
        resp = wrapper.player_update(payload,local)
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"wrong password")
        self.assertEqual(self.getPlayers()[0]['p']['games_played'],0)  #make sure old entry hasnt been update
        
    def test_update_played_invalid_int(self):
        payload = {'username': 'username', 'password': 'password', 'add_to_games_played': -10}
        resp = wrapper.player_update(payload,local)
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"Value to add is <=0")
        self.assertEqual(self.getPlayers()[0]['p']['games_played'],0) #make sure old entry hasnt been update

    
    
    def test_update_score_correct(self):
        payload = {'username': 'username', 'password': 'password', 'add_to_score': 10}
        resp = wrapper.player_update(payload,local)
        self.assertTrue(resp['result'])
        self.assertEqual(resp['msg'],"OK")
        self.assertEqual(self.getPlayers()[0]['p']['total_score'],10)
    
    def test_update_score_wrong_user(self):
        payload = {'username': 'NOTusername', 'password': 'password', 'add_to_score': 10}
        resp = wrapper.player_update(payload,local)
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"user does not exist")
        self.assertEqual(self.getPlayers()[0]['p']['total_score'],0) #make sure old entry hasnt been update
    
    def test_update_score_wrong_pass(self):
        payload = {'username': 'username', 'password': 'NOTpassword', 'add_to_score': 10}
        resp = wrapper.player_update(payload,local)
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"wrong password")
        self.assertEqual(self.getPlayers()[0]['p']['total_score'],0)  #make sure old entry hasnt been update
        
    def test_update_score_invalid_int(self):
        payload = {'username': 'username', 'password': 'password', 'add_to_score': -10}
        resp = wrapper.player_update(payload,local)
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"Value to add is <=0")
        self.assertEqual(self.getPlayers()[0]['p']['total_score'],0) #make sure old entry hasnt been update
    


    def test_update_both_correct(self):
        payload = {'username': 'username', 'password': 'password', 'add_to_score': 10, 'add_to_games_played': 15}
        resp = wrapper.player_update(payload,local)
        self.assertTrue(resp['result'])
        self.assertEqual(resp['msg'],"OK")
        self.assertEqual(self.getPlayers()[0]['p']['total_score'],10)
        self.assertEqual(self.getPlayers()[0]['p']['games_played'],15)
    
    def test_update_bothwrong_user(self):
        payload = {'username': 'NOTusername', 'password': 'password', 'add_to_score': 10, 'add_to_games_played': 15}
        resp = wrapper.player_update(payload,local)
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"user does not exist")
        self.assertEqual(self.getPlayers()[0]['p']['total_score'],0) #make sure old entry hasnt been update
        self.assertEqual(self.getPlayers()[0]['p']['games_played'],0) #make sure old entry hasnt been update
    
    def test_update_both_wrong_pass(self):
        payload = {'username': 'username', 'password': 'NOTpassword', 'add_to_score': 10, 'add_to_games_played': 15}
        resp = wrapper.player_update(payload,local)
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"wrong password")
        self.assertEqual(self.getPlayers()[0]['p']['total_score'],0)  #make sure old entry hasnt been update
        self.assertEqual(self.getPlayers()[0]['p']['games_played'],0) #make sure old entry hasnt been update
        
    def test_update_both_invalid_int(self):
        payload = {'username': 'username', 'password': 'password', 'add_to_score': -10, 'add_to_games_played': 15}
        resp = wrapper.player_update(payload,local)
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"Value to add is <=0")
        self.assertEqual(self.getPlayers()[0]['p']['total_score'],0) #make sure old entry hasnt been update
        self.assertEqual(self.getPlayers()[0]['p']['games_played'],0) #make sure old entry hasnt been update

if __name__ == '__main__':
    unittest.main()