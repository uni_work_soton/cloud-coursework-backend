import unittest
import azure.cosmos as cosmos

import wrapper

import os
db_URI = os.environ['db_URI']
db_id = os.environ['db_id']
db_key = os.environ['db_key']

local = True

class TestPromptDelete(unittest.TestCase):
    client = cosmos.cosmos_client.CosmosClient(db_URI, db_key )
    db_client = client.get_database_client(db_id)
    player_container = db_client.get_container_client('players')
    prompt_container = db_client.get_container_client('prompts')

    @classmethod
    def setUpClass(self) -> None:
        players = self.player_container.read_all_items()
        for p in players:
            self.player_container.delete_item(p['id'],p['id'])
        wrapper.player_register({'username': 'username', 'password': 'password'},local)
        wrapper.player_register({'username': 'username2', 'password': 'password2'},local)
   
    def setUp(self) -> None:
        prompts = self.prompt_container.read_all_items()
        for p in prompts:
            self.prompt_container.delete_item(p['id'],p['id'])
        wrapper.prompt_create({'text': 'This is a correctly formatted prompt!','username': 'username', 'password': 'password'},local)


    def test_delete_correct(self):
        payload = {'id': '0', 'username': 'username', 'password': 'password'}
        resp = wrapper.prompt_delete(payload,local)

        #check the response is correct
        self.assertTrue(resp['result'])
        self.assertEqual(resp['msg'],"OK")

        #check value is not in database
        prompts = list(self.prompt_container.read_all_items())
        self.assertEqual(len(prompts),0)   

    def test_delete_wrong_id(self):
        payload = {'id': '-1', 'username': 'username', 'password': 'password'}
        resp = wrapper.prompt_delete(payload,local)

        #check the response is correct
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"prompt id does not exist")

        #check value is still in database
        prompts = list(self.prompt_container.read_all_items())
        self.assertEqual(len(prompts),1)  

    def test_delete_bad_user(self):
        payload = {'id': '0', 'username': 'NOTusername', 'password': 'password'}
        resp = wrapper.prompt_delete(payload,local)

        #check the response is correct
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"bad username or password")

        #check value is still in database
        prompts = list(self.prompt_container.read_all_items())
        self.assertEqual(len(prompts),1)  
    
    def test_delete_bad_pass(self):
        payload = {'id': '0', 'username': 'username', 'password': 'NOTpassword'}
        resp = wrapper.prompt_delete(payload,local)

        #check the response is correct
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"bad username or password")

        #check value is still in database
        prompts = list(self.prompt_container.read_all_items())
        self.assertEqual(len(prompts),1)  
    
    def test_delete_wrong_user(self) :
        payload = {'id': '0', 'username': 'username2', 'password': 'password2'}
        resp = wrapper.prompt_delete(payload,local)

        #check the response is correct
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"access denied")

        #check value is still in database
        prompts = list(self.prompt_container.read_all_items())
        self.assertEqual(len(prompts),1)  

if __name__ == '__main__':
    unittest.main()