import unittest
import azure.cosmos as cosmos

import wrapper


import os
db_URI = os.environ['db_URI']
db_id = os.environ['db_id']
db_key = os.environ['db_key']

local = True

class TestPlayerLogin(unittest.TestCase):
    client = cosmos.cosmos_client.CosmosClient(db_URI, db_key )
    db_client = client.get_database_client(db_id)
    player_container = db_client.get_container_client('players')
   
    def setUp(self) -> None:
        for p in self.player_container.read_all_items():
            self.player_container.delete_item(p['id'],p['id'])
        payload = {'username': 'username', 'password': 'password'}
        wrapper.player_register(payload,local)
    
    def test_login_correct(self):
        payload = {'username': 'username', 'password': 'password'}
        resp = wrapper.player_login(payload,local)

        self.assertTrue(resp['result'])
        self.assertEqual(resp['msg'],"OK")
    
    def test_login_wrong_password(self):
        payload = {'username': 'username', 'password': 'NOTpassword'}
        resp = wrapper.player_login(payload,local)

        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"Username or password incorrect")
    
    def test_login_wrong_username(self):
        payload = {'username': 'NOTusername', 'password': 'password'}
        resp = wrapper.player_login(payload,local)

        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"Username or password incorrect")

if __name__ == '__main__':
    unittest.main()