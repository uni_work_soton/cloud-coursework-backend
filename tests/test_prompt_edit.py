import unittest
import azure.cosmos as cosmos

import wrapper

import os
db_URI = os.environ['db_URI']
db_id = os.environ['db_id']
db_key = os.environ['db_key']

local = True

class TestPromptEdit(unittest.TestCase):
    client = cosmos.cosmos_client.CosmosClient(db_URI, db_key )
    db_client = client.get_database_client(db_id)
    player_container = db_client.get_container_client('players')
    prompt_container = db_client.get_container_client('prompts')

    @classmethod
    def setUpClass(self) -> None:
        players = self.player_container.read_all_items()
        for p in players:
            self.player_container.delete_item(p['id'],p['id'])
        wrapper.player_register({'username': 'username', 'password': 'password'},local)
   
    def setUp(self) -> None:
        prompts = self.prompt_container.read_all_items()
        for p in prompts:
            self.prompt_container.delete_item(p['id'],p['id'])
        wrapper.prompt_create({'text': 'This is a correctly formatted prompt!','username': 'username', 'password': 'password'},local)


    def test_edit_correct(self):
        payload = {'id': '0', 'text': 'This is a correctly formatted prompt edit!', 'username': 'username', 'password': 'password'}
        resp = wrapper.prompt_edit(payload,local)

        #check the response is correct
        self.assertTrue(resp['result'])
        self.assertEqual(resp['msg'],"OK")

        #check value is changed in database
        prompts = list(self.prompt_container.read_all_items())
        self.assertEqual(len(prompts),1)  
        self.assertEqual(prompts[0]['text'],"This is a correctly formatted prompt edit!")

    def test_edit_wrong_id(self):
        payload = {'id': '-1', 'text': 'This is a correctly formatted prompt edit!', 'username': 'username', 'password': 'password'}
        resp = wrapper.prompt_edit(payload,local)

        #check the response is correct
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"prompt id does not exist")

        #check value is still in database
        prompts = list(self.prompt_container.read_all_items())
        self.assertEqual(len(prompts),1)  
        self.assertEqual(prompts[0]['text'],"This is a correctly formatted prompt!")

    def test_edit_long(self):
        payload = {'text':'this is a wayyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy toooooooooooooooooooo lonnnnnnnnnnnnnggggggggggggggggg', 'username': 'username', 'password': 'password'}
        resp = wrapper.prompt_edit(payload,local)

        #check the response is correct
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"prompt length is <20 or >100 characters")

        #check value is still in database
        prompts = list(self.prompt_container.read_all_items())
        self.assertEqual(len(prompts),1)  
        self.assertEqual(prompts[0]['text'],"This is a correctly formatted prompt!")

    def test_edit_short(self):
        payload = {'text':'this is a too short','username': 'username', 'password': 'password'}
        resp = wrapper.prompt_edit(payload,local)

        #check the response is correct
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"prompt length is <20 or >100 characters")

        #check value is still in database
        prompts = list(self.prompt_container.read_all_items())
        self.assertEqual(len(prompts),1)  
        self.assertEqual(prompts[0]['text'],"This is a correctly formatted prompt!")

    def test_edit_bad_user(self):
        payload = {'id': '0', 'text': 'This is a correctly formatted prompt edit!', 'username': 'NOTusername', 'password': 'password'}
        resp = wrapper.prompt_edit(payload,local)

        #check the response is correct
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"bad username or password")

        #check value is still in database
        prompts = list(self.prompt_container.read_all_items())
        self.assertEqual(len(prompts),1)  
        self.assertEqual(prompts[0]['text'],"This is a correctly formatted prompt!")
    
    def test_edit_bad_pass(self):
        payload = {'id': '0', 'text': 'This is a correctly formatted prompt edit!', 'username': 'username', 'password': 'NOTpassword'}
        resp = wrapper.prompt_edit(payload,local)

        #check the response is correct
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"bad username or password")

        #check value is still in database
        prompts = list(self.prompt_container.read_all_items())
        self.assertEqual(len(prompts),1)  
        self.assertEqual(prompts[0]['text'],"This is a correctly formatted prompt!")
    
    def test_edit_duplicate_user(self) :
        wrapper.prompt_create({'text': 'This is ANOTHER correctly formatted prompt!','username': 'username', 'password': 'password'},local)

        payload = {'id': '0', 'text': 'This is ANOTHER correctly formatted prompt!', 'username': 'username', 'password': 'password'}
        resp = wrapper.prompt_edit(payload,local)

        #check the response is correct
        self.assertFalse(resp['result'])
        self.assertEqual(resp['msg'],"This user already has a prompt with the same text")

        #check values are still correctly in database
        prompts = list(self.prompt_container.read_all_items())
        self.assertEqual(len(prompts),2)  
        self.assertEqual(prompts[0]['text'],"This is a correctly formatted prompt!")
        self.assertEqual(prompts[1]['text'],"This is ANOTHER correctly formatted prompt!")

if __name__ == '__main__':
    unittest.main()