import unittest
import azure.cosmos as cosmos

import wrapper


import os
db_URI = os.environ['db_URI']
db_id = os.environ['db_id']
db_key = os.environ['db_key']

local = True

class TestPlayerLeaderboard(unittest.TestCase):
    client = cosmos.cosmos_client.CosmosClient(db_URI, db_key )
    db_client = client.get_database_client(db_id)
    player_container = db_client.get_container_client('players')
   
    def setUp(self) -> None:
        for p in self.player_container.read_all_items():
            self.player_container.delete_item(p['id'],p['id'])
    
    def test_leaderboard_single(self):
        p1 = {'username': 'username', 'password': 'password'}
        wrapper.player_register(p1,local)
        resp = wrapper.player_leaderboard({'top' : 1},local)
      
        self.assertEqual(len(resp), 1)
        self.assertEqual(resp[0]['id'], 'username')

    def test_leaderboard_alphanumeric(self):
        A = {'username': 'userA', 'password': 'password'}
        B = {'username': 'userB', 'password': 'password'}
        C = {'username': 'userC', 'password': 'password'}
        wrapper.player_register(B,local)
        wrapper.player_register(C,local)
        wrapper.player_register(A,local)
        wrapper.player_update({'username': 'userC', 'password': 'password', "add_to_score" : 5},local)
        resp = wrapper.player_leaderboard({'top' : 3},local)
      
        self.assertEqual(len(resp), 3)
        self.assertEqual(resp[0]['id'], 'userC')
        self.assertEqual(resp[1]['id'], 'userA')
        self.assertEqual(resp[2]['id'], 'userB')

    def test_leaderboard_high_k(self):
        p1 = {'username': 'username', 'password': 'password'}
        wrapper.player_register(p1,local)
        resp = wrapper.player_leaderboard({'top' : 2},local) #asking for more data than is in the database
      
        self.assertEqual(len(resp), 1)
        self.assertEqual(resp[0]['id'], 'username')
    
    def test_leaderboard_low_k(self):
        A = {'username': 'userA', 'password': 'password'}
        B = {'username': 'userB', 'password': 'password'}
        C = {'username': 'userC', 'password': 'password'}
        wrapper.player_register(B,local)
        wrapper.player_register(C,local)
        wrapper.player_register(A,local)
        resp = wrapper.player_leaderboard({'top' : 1},local) #asking for less data than is in the database
      
        self.assertEqual(len(resp), 1)
        self.assertEqual(resp[0]['id'], 'userA')

    def test_leaderboard_empty(self):
        payload = {'top': 1}
        resp = wrapper.player_leaderboard(payload,local)
        
        self.assertTrue(len(resp) == 0)



if __name__ == '__main__':
    unittest.main()