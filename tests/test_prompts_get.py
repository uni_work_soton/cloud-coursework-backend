import unittest
import azure.cosmos as cosmos

import wrapper

import os
db_URI = os.environ['db_URI']
db_id = os.environ['db_id']
db_key = os.environ['db_key']

local = True

class TestPromptsGet(unittest.TestCase):
    client = cosmos.cosmos_client.CosmosClient(db_URI, db_key )
    db_client = client.get_database_client(db_id)
    player_container = db_client.get_container_client('players')
    prompt_container = db_client.get_container_client('prompts')

    @classmethod
    def setUpClass(self) -> None:
        players = self.player_container.read_all_items()
        for p in players:
            self.player_container.delete_item(p['id'],p['id'])
        wrapper.player_register({'username': 'username', 'password': 'password'},local)
        wrapper.player_register({'username': 'username2', 'password': 'password2'},local)
   
    def setUp(self) -> None:
        prompts = self.prompt_container.read_all_items()
        for p in prompts:
            self.prompt_container.delete_item(p['id'],p['id'])
    


    def test_get_prompts_single(self):
        wrapper.prompt_create({'text': 'This is a correctly formatted prompt!','username': 'username', 'password': 'password'},local)
        resp = wrapper.prompts_get({'prompts' : 1},local)
      
        self.assertEqual(len(resp), 1)
        self.assertEqual(resp[0]['text'], 'This is a correctly formatted prompt!')

    def test_get_prompts_high_n(self):
        wrapper.prompt_create({'text': 'This is a correctly formatted prompt!','username': 'username', 'password': 'password'},local)
        resp = wrapper.prompts_get({'prompts' : 2},local) #asking for more data than is in the database
      
        self.assertEqual(len(resp), 1)
        self.assertEqual(resp[0]['text'], 'This is a correctly formatted prompt!')
    
    def test_get_prompts_low_n(self):
        wrapper.prompt_create({'text': 'This is a correctly formatted prompt!','username': 'username', 'password': 'password'},local)
        wrapper.prompt_create({'text': 'This is another correctly formatted prompt!','username': 'username2', 'password': 'password2'},local)

        resp = wrapper.prompts_get({'prompts' : 1},local) #asking for less data than is in the database
      
        self.assertEqual(len(resp), 1)
        self.assertIn(resp[0]['text'], {'This is a correctly formatted prompt!','This is another correctly formatted prompt!'})

    def test_get_prompts_empty(self):
        resp = wrapper.prompts_get({'prompts' : 1},local) 

        self.assertTrue(len(resp) == 0)



    def test_get_player(self):
        wrapper.prompt_create({'text': 'This is a correctly formatted prompt!','username': 'username', 'password': 'password'},local)
        wrapper.prompt_create({'text': 'This is another correctly formatted prompt!','username': 'username', 'password': 'password'},local)
        wrapper.prompt_create({'text': 'This is a third formatted prompt!','username': 'username2', 'password': 'password2'},local)

        resp1 = wrapper.prompts_get({'players' : ['username'] },local)
        resp2 = wrapper.prompts_get({'players' : ['username2'] },local)
        resp3 = wrapper.prompts_get({'players' : ['username','username2'] },local)
        print(f"nya1 {resp1}")
        print(f"nya2 {resp2}")
        print(f"nya3 {resp3}")

        self.assertEqual(len(resp1), 2)
        self.assertEqual(resp1[0]['text'], 'This is a correctly formatted prompt!')
        self.assertEqual(resp1[1]['text'], 'This is another correctly formatted prompt!')

        self.assertEqual(len(resp2), 1)
        self.assertEqual(resp2[0]['text'], 'This is a third formatted prompt!')

        self.assertEqual(len(resp3), 3)
        self.assertEqual(resp3[0]['text'], 'This is a correctly formatted prompt!')
        self.assertEqual(resp3[1]['text'], 'This is another correctly formatted prompt!')
        self.assertEqual(resp3[2]['text'], 'This is a third formatted prompt!')
    
    def test_get_player_empty(self):
        resp1 = wrapper.prompts_get({'players' : ['username'] },local)
        self.assertEqual(len(resp1), 0)

        wrapper.prompt_create({'text': 'This is a correctly formatted prompt!','username': 'username', 'password': 'password'},local)
        resp2 = wrapper.prompts_get({'players' : ['username2']},local)
        self.assertEqual(len(resp2), 0)

if __name__ == '__main__':
    unittest.main()