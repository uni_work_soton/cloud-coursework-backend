import logging
import azure.functions as func
import azure.cosmos as cosmos
import json

import os 
db_URI = os.environ['db_URI']
db_id = os.environ['db_id']
db_key = os.environ['db_key']
player_cont = os.environ['player_container']


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Player update http request recieved')

    client = cosmos.cosmos_client.CosmosClient(db_URI, db_key)
    db_client = client.get_database_client(db_id)
    player_container = db_client.get_container_client(player_cont)

    req_body = req.get_json()

    id_to_check = req_body["username"]
    password_to_check = req_body["password"]

    all_players = list(player_container.query_items( #fetches all players with matching username/id
        query= "SELECT p FROM p WHERE p.id = @id",
        parameters = [{"name" : "@id", "value":id_to_check}],
        enable_cross_partition_query=True
    ))

    if not all_players:
        logging.info("Username not found")
        return func.HttpResponse(json.dumps({"result": False, "msg": "user does not exist" }))

    working_user = all_players[0]["p"]
    
    if password_to_check != working_user["password"]:
        logging.info("Wrong password")
        return func.HttpResponse(json.dumps({"result": False, "msg": "wrong password" }))

    if "add_to_games_played" not in req_body.keys(): #case of only adding score
        add_to_score = req_body["add_to_score"]

        if add_to_score<=0:
            logging.info("Value to add too low")
            return func.HttpResponse(json.dumps({"result": False, "msg": "Value to add is <=0" }))
    
        working_user["total_score"] = working_user["total_score"] + add_to_score
        player_container.replace_item(id_to_check,working_user)

    elif "add_to_score" not in req_body.keys(): #case of only adding games played
        add_to_games_played = req_body["add_to_games_played"]

        if add_to_games_played<=0:
            logging.info("Value to add too low")
            return func.HttpResponse(json.dumps({"result": False, "msg": "Value to add is <=0" }))

        working_user["games_played"] = working_user["games_played"] + add_to_games_played
        player_container.replace_item(id_to_check,working_user)

    else: #case of both
        add_to_score = req_body["add_to_score"]
        add_to_games_played = req_body["add_to_games_played"]

        if add_to_score<=0 or add_to_games_played<=0:
            logging.info("Value to add too low")
            return func.HttpResponse(json.dumps({"result": False, "msg": "Value to add is <=0" }))

        working_user["games_played"] = working_user["games_played"] + add_to_games_played
        working_user["total_score"] = working_user["total_score"] + add_to_score
        player_container.replace_item(id_to_check,working_user)

    return func.HttpResponse(json.dumps({"result" : True, "msg": "OK" }))