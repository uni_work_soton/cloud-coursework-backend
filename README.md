# cloud-coursework-backend

Python backend which utilises azure to create an API for an azure database for a javascript game.

Uses azure functions for each possible interaction with the API.

This was made as a part of a final year module at university, Cloud Application Development.
