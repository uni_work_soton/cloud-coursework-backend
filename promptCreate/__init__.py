import logging
import azure.functions as func
import azure.cosmos as cosmos
import json

import os 
db_URI = os.environ['db_URI']
db_id = os.environ['db_id']
db_key = os.environ['db_key']
prompts_cont = os.environ['prompts_container']
player_cont = os.environ['player_container']

def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Prompt create http request recieved')

    client = cosmos.cosmos_client.CosmosClient(db_URI, db_key)
    db_client = client.get_database_client(db_id)
    prompts_container = db_client.get_container_client(prompts_cont)
    player_container = db_client.get_container_client(player_cont)

    req_body = req.get_json()
    text_to_add = req_body["text"]
    username_to_add = req_body["username"]
    password = req_body["password"]

    all_players = list(player_container.query_items(
        query= "SELECT p FROM p WHERE p.id = @id",
        parameters = [{"name" : "@id", "value":username_to_add}],
        enable_cross_partition_query=True
    ))

    all_prompts_of_username = list(prompts_container.query_items(
        query= "SELECT p FROM p WHERE p.username = @username",
        parameters = [{"name" : "@username", "value":username_to_add}],
        enable_cross_partition_query=True
    ))

    all_prompts = list(prompts_container.query_items(
        query= "SELECT p FROM p",
        enable_cross_partition_query=True
    ))

    #check username and password are ok
    if all_players:
        if password != all_players[0]["p"]["password"]:
            logging.error("password doesnt match username")
            return func.HttpResponse(json.dumps({"result": False, "msg": "bad username or password" }))
    else:
        logging.error("username not found")
        return func.HttpResponse(json.dumps({"result": False, "msg": "bad username or password" }))
    
    #checking if user already has a prompt with same text
    for prompt in all_prompts_of_username:
        if prompt["p"]["text"] == text_to_add:
            logging.error("prompt from same user already exists")
            return func.HttpResponse(json.dumps({"result": False, "msg": "This user already has a prompt with the same text" }))

    #check length
    if len(text_to_add)<20 or len(text_to_add)>100:
        logging.error("bad prompt length")
        return func.HttpResponse(json.dumps({"result": False, "msg": "prompt length is <20 or > 100 characters" }))

    #ok!
    ids = []
    for prompt in all_prompts:
        ids.append(int(prompt["p"]["id"]))
    
    #there is already prompts in container
    if ids:
        prompts_container.create_item({"id": str(max(ids)+1), "username": username_to_add, "text": text_to_add})
    else:
        prompts_container.create_item({"id": "0", "username": username_to_add, "text": text_to_add})

    return func.HttpResponse(json.dumps({"result" : True, "msg": "OK" }))