import logging
import azure.functions as func
import azure.cosmos as cosmos
import json
import random

import os 
db_URI = os.environ['db_URI']
db_id = os.environ['db_id']
db_key = os.environ['db_key']
prompts_cont = os.environ['prompts_container']
player_cont = os.environ['player_container']

def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Prompt get http request recieved')

    client = cosmos.cosmos_client.CosmosClient(db_URI, db_key)
    db_client = client.get_database_client(db_id)
    prompts_container = db_client.get_container_client(prompts_cont)

    req_body = req.get_json()

    all_prompts = list(prompts_container.query_items(
        query= "SELECT p FROM p",
        enable_cross_partition_query=True
    ))


    #cleans up prompts for use/output
    all_prompts = [x["p"] for x in all_prompts]
    all_prompts_minus_hiddens = []
    for prompt in all_prompts:
        all_prompts_minus_hiddens.append({key:val for key, val in prompt.items() if key[0]!="_"})

    output_prompts = []
    if "prompts" in req_body.keys():
        #return n number of prompts drawn randomly
        n = int(req_body["prompts"])
        if n>len(all_prompts):
            output_prompts = all_prompts_minus_hiddens
        else:
            for _ in range(n):
                prompt_index = random.randint(0,n-1)
                output_prompts.append(all_prompts_minus_hiddens[prompt_index])
    else:
        #return list of all prompts made by usernames in array
        usernames_to_output = req_body["players"]
        for prompt in all_prompts_minus_hiddens:
            if prompt["username"] in usernames_to_output:
                output_prompts.append(prompt)
    return(func.HttpResponse(json.dumps(output_prompts)))

        