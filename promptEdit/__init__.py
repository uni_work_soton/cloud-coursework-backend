import logging
import azure.functions as func
import azure.cosmos as cosmos
import json

import os 
db_URI = os.environ['db_URI']
db_id = os.environ['db_id']
db_key = os.environ['db_key']
prompts_cont = os.environ['prompts_container']
player_cont = os.environ['player_container']

def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Prompt edit http request recieved')

    client = cosmos.cosmos_client.CosmosClient(db_URI, db_key)
    db_client = client.get_database_client(db_id)
    prompts_container = db_client.get_container_client(prompts_cont)
    player_container = db_client.get_container_client(player_cont)

    req_body = req.get_json()
    text_to_change = req_body["text"]
    username = req_body["username"]
    password = req_body["password"]
    prompt_id = str(req_body["id"])

    all_players = list(player_container.query_items(
        query= "SELECT p FROM p WHERE p.id = @id",
        parameters = [{"name" : "@id", "value":username}],
        enable_cross_partition_query=True
    ))

    all_prompts_of_username = list(prompts_container.query_items(
        query= "SELECT p FROM p WHERE p.username = @username",
        parameters = [{"name" : "@username", "value":username}],
        enable_cross_partition_query=True
    ))

    all_prompts = list(prompts_container.query_items(
        query= "SELECT p FROM p",
        enable_cross_partition_query=True
    ))

    #check username and password are ok
    if all_players:
        if password != all_players[0]["p"]["password"]:
            logging.error("password doesnt match username")
            return func.HttpResponse(json.dumps({"result": False, "msg": "bad username or password" }))
    else:
        logging.error("username not found")
        return func.HttpResponse(json.dumps({"result": False, "msg": "bad username or password" }))

    #check if new prompt already exists on user
    for prompt in all_prompts_of_username:
        if prompt["p"]["text"] == text_to_change:
            logging.error("prompt from same user already exists")
            return func.HttpResponse(json.dumps({"result": False, "msg": "This user already has a prompt with the same text" }))

    #check length
    if len(text_to_change)<20 or len(text_to_change)>100:
        logging.error("bad prompt length")
        return func.HttpResponse(json.dumps({"result": False, "msg": "prompt length is <20 or > 100 characters" }))

    #check if prompt id exists
    ids = []
    for prompt in all_prompts:
        ids.append(prompt["p"]["id"])
    
    if prompt_id not in ids:
        logging.error("prompt doesnt exist")
        return func.HttpResponse(json.dumps({"result": False, "msg": "prompt id does not exist" }))

    #ok!
    prompt_to_edit = [prompt for prompt in all_prompts if prompt["p"]["id"] == prompt_id][0]["p"]
    prompt_to_replace = {"id": prompt_to_edit["id"], "username": prompt_to_edit["username"], "text":text_to_change}
    prompts_container.replace_item(prompt_to_edit,prompt_to_replace)

    return func.HttpResponse(json.dumps({"result" : True, "msg": "OK" }))

