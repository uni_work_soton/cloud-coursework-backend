import logging
import azure.functions as func
import azure.cosmos as cosmos
import json

import os 
db_URI = os.environ['db_URI']
db_id = os.environ['db_id']
db_key = os.environ['db_key']
player_cont = os.environ['player_container']

def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Player register http request recieved')

    client = cosmos.cosmos_client.CosmosClient(db_URI, db_key)
    db_client = client.get_database_client(db_id)
    player_container = db_client.get_container_client(player_cont)

    req_body = req.get_json()
    id_to_add = req_body["username"]
    password_to_add = req_body["password"]

    all_players = list(player_container.query_items(
        query= "SELECT p FROM p WHERE p.id = @id",
        parameters = [{"name" : "@id", "value":id_to_add}],
        enable_cross_partition_query=True
    ))

    if len(all_players)>0:
        logging.error("Player already exists in db")
        return func.HttpResponse(json.dumps({"result": False, "msg": "Username already exists"}))

    if len(id_to_add)<4 or len(id_to_add)>16:
        logging.error("Player username is wrong size")
        return func.HttpResponse(json.dumps({"result": False, "msg": "Username less than 4 characters or more than 16 characters"}))
    
    if len(password_to_add)<8 or len(password_to_add)>24:
        logging.error("Player password is wrong size")
        return func.HttpResponse(json.dumps({"result": False, "msg": "Password less than 8 characters or more than 24 characters"}))

    
    logging.info("Adding player to db")
    player_container.create_item({"id": id_to_add, "password": password_to_add, "games_played": 0, "total_score":0})
    return func.HttpResponse(json.dumps({"result" : True, "msg": "OK" }))