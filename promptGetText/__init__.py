import logging
import azure.functions as func
import azure.cosmos as cosmos
import json
import re

import os 
db_URI = os.environ['db_URI']
db_id = os.environ['db_id']
db_key = os.environ['db_key']
prompts_cont = os.environ['prompts_container']
player_cont = os.environ['player_container']

def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Prompt get text http request recieved')

    client = cosmos.cosmos_client.CosmosClient(db_URI, db_key)
    db_client = client.get_database_client(db_id)
    prompts_container = db_client.get_container_client(prompts_cont)

    req_body = req.get_json()
    word = req_body["word"]
    exact = req_body["exact"]

    all_prompts = list(prompts_container.query_items(
        query= "SELECT p FROM p",
        enable_cross_partition_query=True
    ))
    
    #cleans up prompts for use/output
    all_prompts = [x["p"] for x in all_prompts]
    all_prompts_minus_hiddens = []
    for prompt in all_prompts:
        all_prompts_minus_hiddens.append({key:val for key, val in prompt.items() if key[0]!="_"})

    output_prompts = []
    #iterate through prompt for searching
    for prompt in all_prompts_minus_hiddens:
        text = prompt["text"]
        if exact:
            text = text.lower()
            word = word.lower()
        logging.info("text: " + text)

        split_text = re.split(r'\b',text)
        
        logging.info(split_text)
        if word in split_text:
            output_prompts.append(prompt)

    return(func.HttpResponse(json.dumps(output_prompts)))
