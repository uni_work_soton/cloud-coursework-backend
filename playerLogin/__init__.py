import logging
import azure.functions as func
import azure.cosmos as cosmos
import json

import os 
db_URI = os.environ['db_URI']
db_id = os.environ['db_id']
db_key = os.environ['db_key']
player_cont = os.environ['player_container']

def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Player login http request recieved')

    client = cosmos.cosmos_client.CosmosClient(db_URI, db_key)
    db_client = client.get_database_client(db_id)
    player_container = db_client.get_container_client(player_cont)

    req_body = req.get_json()

    id_to_check = req_body["username"]
    password_to_check = req_body["password"]

    all_players = list(player_container.query_items(
        query= "SELECT p FROM p WHERE p.id = @id",
        parameters = [{"name" : "@id", "value":id_to_check}],
        enable_cross_partition_query=True
    ))

    '''
    all_players is a list containing either 0 or 1 elements depending on if is is found
    when inside is a dict with "p" as the key, the value is the element in the container
    '''

    if all_players:
        if password_to_check == all_players[0]["p"]["password"]:
            logging.info("Player username and password match")
            return func.HttpResponse(json.dumps({"result": True , "msg" : "OK"}))
    logging.error("Player username and password not found")

    return func.HttpResponse(json.dumps({"result": False , "msg": "Username or password incorrect"}))