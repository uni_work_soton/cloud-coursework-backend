import logging
import azure.functions as func
import azure.cosmos as cosmos
import json

import os 
db_URI = os.environ['db_URI']
db_id = os.environ['db_id']
db_key = os.environ['db_key']
player_cont = os.environ['player_container']

def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Player leaderboard ')

    client = cosmos.cosmos_client.CosmosClient(db_URI, db_key)
    db_client = client.get_database_client(db_id)
    player_container = db_client.get_container_client(player_cont)

    req_body = req.get_json()

    top_k = req_body["top"]

    #fetches all players 
    all_players = list(player_container.query_items( 
        query= "SELECT p FROM p",
        enable_cross_partition_query=True
    ))

    #each entry is a key-value of p[entry], this gets the entry
    all_players = [x["p"] for x in all_players] 

    #gets rid of password field as this wont be in return entries. this could be
    #done in the sql query, but i cba to look into more documentation when this works fine
    #also removes 'hidden' attributes that start with a _
    all_players_minus_pass = []
    for player in all_players:
        all_players_minus_pass.append({key:val for key, val in player.items() if key != "password" and key[0]!="_"})

    #generates list of names by top scores to worst scores
    leaderboard = {}
    for player in all_players_minus_pass:
        leaderboard[player["id"]] = player["total_score"]
    #sorts leaderboard by value, then by key
    sorted_leaderboard = sorted(leaderboard, key = lambda x: (-leaderboard[x], x))
    #gets the first k values
    sorted_leaderboard = sorted_leaderboard[:top_k]
    
    
    sorted_entry = []
    for id in sorted_leaderboard:
        for entry in all_players_minus_pass:
            if not "username" in entry.keys(): #we're updating the key names to fit with the spec...
                if entry["id"] == id:
                    #match found
                    break

        #replace id and total score with username and score. pushed games played to back of dict (dict is ordered)
        
        entry["username"] = entry["id"]
        entry["score"] = entry["total_score"]
        gamesPlayed = entry["games_played"]

        entry.pop("id")
        entry.pop("total_score")

        entry.pop("games_played")
        entry["games_played"] = gamesPlayed
        sorted_entry.append(entry)

    return func.HttpResponse(json.dumps(sorted_entry))